<?php
/**
 * Fired during plugin deactivation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 * @author     WPEka Club <support@wpeka.com>
 */
class Analytics_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
