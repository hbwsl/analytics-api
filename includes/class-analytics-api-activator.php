<?php
/**
 * Fired during plugin activation
 *
 * @link       https://club.wpeka.com
 * @since      1.0.0
 *
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Analytics_Api
 * @subpackage Analytics_Api/includes
 * @author     WPEka Club <support@wpeka.com>
 */
class Analytics_Api_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
